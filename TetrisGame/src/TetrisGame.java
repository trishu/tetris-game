package com.mahimakothari.TetrisGame;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class TetrisGame extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -707625040974021584L;

	private final Point[][][] gamePoint = {
			{
				//I
				{new Point(0,1), new Point(1,1), new Point(2,1), new Point(3,1)},
				{new Point(1,0), new Point(1,1), new Point(1, 2), new Point(1, 3)},
				{new Point(0,1), new Point(1,1), new Point(2,1), new Point(3,1)},
				{new Point(1,0), new Point(1,1), new Point(1, 2), new Point(1, 3)}
				
			},
			{
				//J
				{new Point(0,1), new Point(1,1), new Point(2,1), new Point(2,0)},
				{new Point(1,0), new Point(1,1), new Point(1, 2), new Point(2, 2)},
				{new Point(0,1), new Point(1,1), new Point(2,1), new Point(0,2)},
				{new Point(1,0), new Point(1,1), new Point(1, 2), new Point(0, 0)}
				
			},
			{
				//L
				{new Point(0,1), new Point(1,1), new Point(2,1), new Point(2,0)},
				{new Point(1,0), new Point(1,1), new Point(1, 2), new Point(2, 2)},
				{new Point(0,1), new Point(1,1), new Point(2,1), new Point(0,0)},
				{new Point(1,0), new Point(1,1), new Point(1, 2), new Point(2, 0)}
				
			},
			{
				//0
				{new Point(0,0), new Point(0,1), new Point(1, 0), new Point(1, 1)},
				{new Point(0,0), new Point(0,1), new Point(1, 0), new Point(1, 1)},
				{new Point(0,0), new Point(0,1), new Point(1, 0), new Point(1, 1)},
				{new Point(0,0), new Point(0,1), new Point(1, 0), new Point(1, 1)},

			},
			{
				//T
				{new Point(0,0), new Point(0,1), new Point(0, 2), new Point(1, 1)},
				{new Point(0,2), new Point(1, 2), new Point(2, 2), new Point(1, 1)},
				{new Point(2,0), new Point(2,1), new Point(2, 2), new Point(1, 1)},
				{new Point(0,0), new Point(1, 0), new Point(2, 0), new Point(1, 1)},
			},
			{
				//|_
				//	|

				{new Point(0,1), new Point(1,1), new Point(1, 0), new Point(2, 0)},
				{new Point(0,0), new Point(0, 1), new Point(1, 1), new Point(1, 2)},
				{new Point(0,1), new Point(1,1), new Point(1, 2), new Point(2, 2)},
				{new Point(2,0), new Point(2, 1), new Point(1, 1), new Point(1, 2)},	
			}
	};

	private final Color[] gameColor = {
			Color.CYAN,
			Color.magenta,
			Color.orange,
			Color.yellow,
			Color.red,
			Color.green
	};
	
	private Point pt;
	private int currentPiece;
	private int rotation;
	private ArrayList<Integer> nextPiece = new ArrayList<Integer>();
	private long score;
	private Color[][] well;
	
	
	private void init() {
		well = new Color[12][24];
		for(int i = 0; i < 12; i++) {
			for(int j = 0; j < 23; j++) {
				if(i == 0 || i == 11 || j == 22 || j == 0) {
					well[i][j] = Color.gray;
				}
				else {
					well[i][j] = Color.black;
				}
			}
		}
		newPiece();
	}
	
	public void newPiece() {
		pt = new Point(5, 2);
		rotation = 0;
		if(nextPiece.isEmpty()) {
			Collections.addAll(nextPiece, 0,1,2,3,4,5);
		}
		Collections.shuffle(nextPiece);
		//System.out.println(nextPiece);
		currentPiece = nextPiece.get(0);
	}
	
	private boolean collidesAt(int x, int y, int rotation) {
		for(Point p : gamePoint[currentPiece][rotation]) {
			if(well[p.x + x][p.y + y] != Color.black) {
				return true;
			}
		}
		return false;
	}
	
	private void rotate(int i) {
		int newRotation = (rotation + i)%4;
		if(newRotation < 0) {
			newRotation = 3;
		}
		if(!collidesAt(pt.x, pt.y, newRotation)) {
			rotation = newRotation;
		}
		repaint();
	}
	
	public void move(int i) {
		if(!collidesAt(pt.x + i, pt.y, rotation)) {
			pt.x += i;
		}
		repaint();
	}
	
	public void drop() {
		if(!collidesAt(pt.x, pt.y, rotation)) {
			pt.y += 1;
		}
		else {
			fixToWell();
		}
		repaint();
	}
	
	public void drop_end() {
		while(!collidesAt(pt.x, pt.y, rotation)) {
			pt.y += 1;
		}
		fixToWell();
		repaint();
	}
	
	public void fixToWell() {
		for(Point p: gamePoint[currentPiece][rotation]) {
			well[pt.x + p.x][pt.y + p.y - 1] = gameColor[currentPiece];
		}
		clearRows();
		newPiece();
	}
	
	public void deleteRow(int row) {
		for(int j = row - 1; j > 0; j--) {
			for(int i = 1; i < 11; i++) {
				well[i][j + 1] = well[i][j];
			}
		}
	}
	
	public void clearRows() {
		boolean gap;
		int numClear = 1;
		for(int j = 21; j > 0; j--) {
			gap = false;
			for(int i = 1; i < 11; i++) {
				if(well[i][j] == Color.black) {
					gap = true;
					break;
				}
			}
			if(!gap) {
				deleteRow(j);
				j += 1;
				numClear += 1;
			}
		}
		switch(numClear) {
		case 1:
			score += 100;
			break;
		case 2:
			score += 300;
			break;
		case 3:
			score += 500;
			break;
		case 4:
			score += 800;
			break;
		default:
			break;
		}
	}
	
	private void drawPiece(Graphics g) {
		g.setColor(gameColor[currentPiece]);
		for(Point p: gamePoint[currentPiece][rotation]) {
			g.fillRect((p.x + pt.x)*26,(pt.y + p.y - 1)*26, 25, 25);
		}
	}
	
	public void paintComponent(Graphics g) {
		g.fillRect(0, 0, 26*12, 26*23);
		for(int i = 0; i < 12; i++) {
			for(int j = 0; j < 23; j++) {
				g.setColor(well[i][j]);
				g.fillRect(26*i, 26*j, 25, 25);
			}
		}
		g.setColor(Color.WHITE);
		g.drawString("score is : " + score, 19*10, 20);
		drawPiece(g);
	}
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ImageIcon img = new ImageIcon("/icon/tetris-icon.png");
		JFrame f = new JFrame("Tetris Game");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(12*26, 26*23);
		f.setVisible(true);
		f.setIconImage(img.getImage());
		
		
		final TetrisGame game = new TetrisGame();
		game.init();
		f.add(game);
		
		f.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_UP:
					game.rotate(-1);
					break;
				case KeyEvent.VK_DOWN:
					game.rotate(1);
					break;
				case KeyEvent.VK_LEFT:
					game.move(-1);
					break;
				case KeyEvent.VK_RIGHT:
					game.move(1);
					break;
				case KeyEvent.VK_SPACE:
					game.drop();
					game.score += 1;
					break;
				case KeyEvent.VK_ENTER:
					game.drop_end();
					game.score += 5;
					break;
				}
				
			}
		});
		new Thread() {
			public void run() {
				while(true) {
					try {
						Thread.sleep(1000);
					}
					catch(InterruptedException e){
						e.printStackTrace();
					}
					game.drop();
				}
			}
		}.start();
		
	}
 
}
