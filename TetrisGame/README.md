# Tetris Game

Java Implementation of Tetris Game

## How to Play
- Left arrow key -> to move the shape to left
- Right arrow key -> to move the shape to right
- Up arrow key -> for changing shape rotation
- Down arrow key -> for changing shape rotation
- Space bar -> for moving the shape down rapidly
- Enter -> for direct droping the shape

## Download
<a href="TetrisGame/Download/tetris.jar"><img src = "TetrisGame/ScreenShots/tetris-icon.png" height="200" width="200"/></a>

## ScreenShot
<img src="TetrisGame/ScreenShots/tetris-game.png">
