# Brick Breaker

- Java Implementation of Brick Breaker Game

## Controls

- Left arrow key : move the paddle to left
- Right arrow key : move the paddle to right
- Enter key : to restart the game after you loss or won

## Download

<a href="/Download/BrickBreaker.jar"><img src="icon/brickbreaker-icon.jpeg" height="170" width="170"/></a>

- click on above icon
- it will download BrickBreaker.jar file
- Right click on this file & go to permission & check the option of 'Allow executing file as program'
- Now, just double click on file & start playing

## ScreenShots

<img src="screenShots/BrickBreaker-game.png" />
<img src="screenShots/end-game.png">